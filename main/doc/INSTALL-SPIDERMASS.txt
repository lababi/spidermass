For accessing the ChemSpider Online Services, you need a (free) token.

You can get it by registering at http://www.chemspider.com

Please copy the token to a file called
chemspider-token.txt
and put it into the main directory.

Sometimes the SOAP request to the ChemSpider Database are taking longer and return (too) many results. 

The most efficient identifications are done by 1) SpiderMass Database search and 2) De-Novo search.