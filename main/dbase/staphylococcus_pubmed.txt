 delta-Hemolysin
 delta-Lysin
 Hld protein 
 staph alpha-lysin
 Staph alpha-toxin-lysin
 Staphylococcal alpha-toxin 
 staph enterotoxin A
 Staphylococcal enterotoxin A
 Enterotoxin A, staphylococcal 
 toxic-shock toxin
 Staph enterotoxin F
 TSST-1 
 TSST-1
 Staphylococcal Enterotoxin F
 NIOSH/KA8083000 
 Staphylococcus exotoxin
 Exotoxin, staphylococcus
 NIOSH/LF3060000 
 SEB superantigen
 Staph enterotoxin B
 Staphylococcal enterotoxin B 
 Toxins, entero-, A
 Staphylococcal enterotoxin A
 Enterotoxin A, staphylococcal 
 Staphylococcus aureus
 Staphylococcus aureus (Micrococcaceae)
 68583-38-0
 Staphylokokkentoxin
 Staphylococcus toxin
 Staphylokokkentoxin [German] 
 Nora protein, staph
 Nora protein, staphylococcus
 Protein (Saphylococcus aureus clone pTUS829 gene norA) 
 Heptocoagulase
 Peptocoagulase
 Plasmocoagulase 
 Heptocoagulase
 Peptocoagulase
 Plasmocoagulase 
 EDIN
 Epidermal cell differentiation inhibitor
 Protein EDIN (Staphylococcus aureus strain E-1) 
 Staphylokinase
 auR protein, Staphylococcus aureus
 Kinase(enzyme-activating), staphylo 
 Toxins, entero-, A
 Staphylococcal enterotoxin A
 Enterotoxin A, staphylococcal 
 Lukf protein component
 Leukocidin F-component protein
 Leukocidin (Staphylococcus aureus clone pFRK92 gene lukF component F precursor) 
 Vga protein
 Vga gene product
 Vgab gene product 
 Staph enterotoxin B
 Toxins, entero-, B
 Staphylococcal enterotoxin B 
 Leucocidin
 Leukocidin
 Pseudomonas aeruginos cytotoxin 
 Leucocidin
 Leukocidin
 Pseudomonas aeruginos cytotoxin 
 Staphylococcus albus, dried
 NIOSH/WI0195800
 WI01958000
 Qaca protein
 Efflux protein A
 Efpa gene product 
 Staphylococcus aureus, lysate
 EINECS 295-870-3
 92129-70-9
 Toxoids, Staphylococcus aureus
 EINECS 308-524-4
 98072-69-6
 Staphylococcus aureus lysozyme
 NIOSH/WI0195850
 WI01958500
 Staphylotoxin
 Staphylococcal alpha-toxin
 alpha-Staphylotoxin (9CI) 
 Staphylotoxin
 alpha-Staphylotoxin
 Staphylococcal alpha-toxin 
 Proteins, A, Staphylococcus aureus
 EINECS 297-246-6
 93384-36-2
 Nuclease, of Staphylococcus aureus
 NSC260544
 NSC-260544
 EINECS 259-611-8
 Proteinase, Staphylococcus aureus serine
 55354-32-0
 Protein A
 Staphylococcal Protein A
 CCRIS 4112 
 EINECS 297-504-8
 Toxins, entero-, A, Staphylococcus aureus
 93572-74-8
 Toxin B, entero-, (Staphylococcus reduced)
 11100-45-1
 12585-40-9
 EINECS 297-260-2
 Toxins, entero-, D, Staphylococcus aureus
 93384-49-7
 Enterotoxin B (Staphylococcus aureus) 239
 210293-63-3
 EINECS 297-261-8
 Toxins, entero-, E, Staphylococcus aureus
 93384-50-0
 EINECS 297-505-3
 Toxins, entero-, B, Staphylococcus aureus
 93572-75-9
 beta-Hemolysin (Staphylococcus aureus reduced)
 121890-76-4
 EINECS 297-259-7
 Toxins, entero-, C, Staphylococcus aureus
 93384-48-6
 NH2
 ACE
 AMINO GROUP
 NH2
 ACETYL GROUP
 ACE
 succinic acid
 SIN
 potassium ion
 Thiocyanate ion
 SCN
 guanosine-5'-monophosphate
 5GP
 potassium ion
 Sulfate ion
 SO4
 chloride ion
 CL
 glycerol
 GOL
 adenosine-5'-triphosphate
 ATP
 chloride ion
 CL
 Sulfate ion
 SO4
 TRIETHYLENE GLYCOL
 PGE
 BIOTINYL-5-AMP
 BT5
 biotin
 BTN
 (3as,4s,6ar)-4-(5-{1-[4-(6-Amino-9h-Purin-9-Yl)butyl]-1h-1,2,3-Triazol-4-Yl}pentyl)tetrahydro-1h-Thieno[3,4-D]imidazol-2(3h)-One
 32G
 glycerol
 GOL
 glycerol
 GOL
 Acetate Ion
 ACT
 Acetate Ion
 ACT
 pyrophosphate 2-
 POP
 Sulfate ion
 SO4
 maltose
 mal
 potassium ion
 chloride ion
 CL
 TRIETHYLENE GLYCOL
 PGE
 virginiamycin m1
 VIR
 PEG
 1,2-ethanediol
 EDO
 chloride ion
 CL
 sodium ion
 na
 Sulfate ion
 SO4
 chloride ion
 CL
 sodium ion
 na
 1,2-ethanediol
 EDO
 Sulfate ion
 SO4
 ACETYL COENZYME A
 ACO
 TETRAETHYLENE GLYCOL
 PG4
 Cytidine-5'-monophosphate
 C5P
 Flavin-adenine dinucleotide
 FAD
 coenzyme A
 CoA
 chloride ion
 CL
 PEG
 3,6,9,12,15,18-Hexaoxaicosane-1,20-diol
 P33
 potassium ion
 CEFOTAXIME GROUP
 CEF
 chloride ion
 CL
 0Y5
 4-{[(3s)-3-(5-Methyl-2,4-Dioxo-3,4-Dihydropyrimidin-1(2h)-Yl)piperidin-1-Yl]methyl}-2-[3-(Trifluoromethyl)phenoxy]benzoic Acid
 1,2-ethanediol
 EDO
 chloride ion
 CL
 magnesium ion
 MG
 Flavin-adenine dinucleotide
 FAD
 Flavin-adenine dinucleotide
 FAD
 coenzyme A
 CoA
 chloride ion
 CL
 magnesium ion
 MG
 Flavin-adenine dinucleotide
 FAD
 coenzyme A
 CoA
 chloride ion
 CL
 magnesium ion
 MG
 Flavin-adenine dinucleotide
 FAD
 coenzyme A
 CoA
 chloride ion
 CL
 magnesium ion
 MG
 CAJ
 Ethyl 5-[3-[[(2r)-4-[[[(2r,3s,4r,5r)-5-(6-Aminopurin-9-Yl)-4-Oxidanyl-3-Phosphonooxy-Oxolan-2-Yl]methoxy-Oxidanyl-Phosphoryl]oxy-Oxidanyl-Phosphoryl]oxy-3,3-Dimethyl-2-Oxidanyl-Butanoyl]amino]propanoylamino]pentanoate
 chloride ion
 CL
 CAJ
 Ethyl 5-[3-[[(2r)-4-[[[(2r,3s,4r,5r)-5-(6-Aminopurin-9-Yl)-4-Oxidanyl-3-Phosphonooxy-Oxolan-2-Yl]methoxy-Oxidanyl-Phosphoryl]oxy-Oxidanyl-Phosphoryl]oxy-3,3-Dimethyl-2-Oxidanyl-Butanoyl]amino]propanoylamino]pentanoate
 magnesium ion
 MG
 Flavin-adenine dinucleotide
 FAD
 [[(2r,3s,4r,5r)-5-(6-Aminopurin-9-Yl)-4-Oxidanyl-3-Phosphonooxy-Oxolan-2-Yl]methoxy-Oxidanyl-Phosphoryl][(3r)-2,2-Dimethyl-3-Oxidanyl-4-Oxidanylidene-4-[[3-Oxidanylidene-3-[4-(Phenylsulfonyl)butylamino]propyl]amino]butyl] Hydrogen Phosphate
 CA8
 chloride ion
 CL
 magnesium ion
 MG
 Flavin-adenine dinucleotide
 FAD
 [[(2r,3s,4r,5r)-5-(6-Aminopurin-9-Yl)-4-Oxidanyl-3-Phosphonooxy-Oxolan-2-Yl]methoxy-Oxidanyl-Phosphoryl][(3r)-2,2-Dimethyl-4-[[3-(4-Methylsulfonylbutylamino)-3-Oxidanylidene-Propyl]amino]-3-Oxidanyl-4-Oxidanylidene-Butyl] Hydrogen Phosphate
 CA6
 [[(2r,3s,4r,5r)-5-(6-Aminopurin-9-Yl)-4-Oxidanyl-3-Phosphonooxy-Oxolan-2-Yl]methoxy-Oxidanyl-Phosphoryl][(3r)-2,2-Dimethyl-4-[[3-(4-Methylsulfonylbutylamino)-3-Oxidanylidene-Propyl]amino]-3-Oxidanyl-4-Oxidanylidene-Butyl] Hydrogen Phosphate
 CA6
 chloride ion
 CL
 magnesium ion
 MG
 Flavin-adenine dinucleotide
 FAD
 Phosphate ion
 PO4
 G2H
 sodium ion
 na
 3-phosphoglyceric acid
 3PG
 DTT
 2-phosphoglyceric acid
 2PG
 sodium ion
 na
 citric acid
 cit
 sn-glycerol-3-phosphate
 g3p
 acetoacetyl-coenzyme a
 CAA
 3-hydroxy-3-methylglutaryl-coenzyme A
 HMG
 acetoacetyl-coenzyme a
 CAA
 3-hydroxy-3-methylglutaryl-coenzyme A
 HMG
 glycerol
 GOL
 Unknown Ligand
 UNL
 chloride ion
 CL
 MINOCYCLINE HYDROCHLORIDE
 DSSTox_CID_24545
 DSSTox_RID_80306 
 minocycline
 DSSTox_CID_25033
 DSSTox_RID_80650 
 MINOCYCLINE HYDROCHLORIDE
 DSSTox_CID_24545
 DSSTox_RID_80306 
 MINOCYCLINE HYDROCHLORIDE
 DSSTox_CID_24545
 DSSTox_RID_80306 
 1,2-ethanediol
 EDO
 glycerol
 GOL
 malonyl-coenzyme a
 MLC
 malonyl-coenzyme a
 MLC
 chloride ion
 CL
 MES
 2-(N-MORPHOLINO)-ETHANESULFONIC ACID
 Acetate Ion
 ACT
 zinc ion
 ZN
 0WE
 N-Methyl-N-[(3-Methyl-1-Benzofuran-2-Yl)methyl]-3-(7-Oxo-5,6,7,8-Tetrahydro-1,8-Naphthyridin-3-Yl)propanamide
 [[(2r,3s,4r,5r)-5-(3-Aminocarbonyl-4h-Pyridin-1-Yl)-3,4-Bis(Oxidanyl)oxolan-2-Yl]methoxy-Oxidanyl-Phosphoryl] [(2r,3s,4r,5r)-5-(6-Aminopurin-9-Yl)-4-Oxidanyl-3-Phosphonooxy-Oxolan-2-Yl]methyl Hydrogen Phosphate
 0WD
 BCP9000928
 10118-90-8
 glycerol
 GOL
 glycerol
 GOL
 MINOCYCLINE HYDROCHLORIDE
 MLS002548863
 SMR001906766
 zinc ion
 ZN
 chloride ion
 CL
 MINOCYCLINE HYDROCHLORIDE
 Pharmakon1600-01500414
 NSC757120 
 EPE
 nadp nicotinamide-adenine-dinucleotide phosphate
 NAP
 chloride ion
 CL
 glycerol
 GOL
 Flavin-adenine dinucleotide
 FAD
 3-[(6-Chloro[1,3]thiazolo[5,4-B]pyridin-2-Yl)methoxy]-2,6-Difluorobenzamide
 9PC
 guanosine-5'-diphosphate
 GDP
 calcium ion
 CA
 calcium ion
 CA
 guanosine-5'-diphosphate
 GDP
 guanosine-5'-diphosphate
 GDP
 calcium ion
 CA
 magnesium ion
 MG
 magnesium ion
 MG
 magnesium ion
 MG
 magnesium ion
 MG
 TETRAETHYLENE GLYCOL
 PG4
 TETRAETHYLENE GLYCOL
 PG4
 Phosphate ion
 PO4
 chloride ion
 CL
 Cadmium ion
 CD
 Bicarbonate ion
 BCT
 (2r)-2-[(1r)-1-{[(2z)-2-(5-Amino-1,2,4-Thiadiazol-3-Yl)-2-(Hydroxyimino)acetyl]amino}-2-Oxoethyl]-5-({2-Oxo-1-[(3r)-Pyrrolidin-3-Yl]-2,5-Dihydro-1h-Pyrrol-3-Yl}methyl)-3,6-Dihydro-2h-1,3-Thiazine-4-Carboxylic Acid
 RB6
 glycerol
 GOL
 1-({[(S)-Hydroxy(Phosphonooxy)phosphoryl]oxy}acetyl)-L-Proline
 2P0
 formic acid
 FMT
 (3r)-3-Hydroxy-5-{[(R)-Hydroxy(Phosphonooxy)phosphoryl]oxy}-3-Methylpentanoic Acid
 DP6
 1-({[(S)-Hydroxy(Phosphonooxy)phosphoryl]oxy}acetyl)-L-Proline
 2P0
 formic acid
 FMT
 glycerol
 GOL
 glycerol
 GOL
 formic acid
 FMT
 (3r)-3-Hydroxy-5-{[(R)-Hydroxy(Phosphonooxy)phosphoryl]oxy}-3-Methylpentanoic Acid
 DP6
 phosphothiophosphoric acid-adenylate ester
 AGS
 phosphothiophosphoric acid-adenylate ester
 AGS
 (3r)-3-(Fluoromethyl)-3-Hydroxy-5-{[(S)-Hydroxy(Phosphonooxy)phosphoryl]oxy}pentanoic Acid
 FM0
 phosphothiophosphoric acid-adenylate ester
 AGS
 (3r)-3-(Fluoromethyl)-3-Hydroxy-5-{[(S)-Hydroxy(Phosphonooxy)phosphoryl]oxy}pentanoic Acid
 FM0
 glycerol
 GOL
 benzamidine
 BEN
 phosphoaminophosphonic acid-adenylate ester
 ANP
 N-((2r,4s)-2-Butyl-5-Methyl-4-(3-(5-Methylpyridin-2-Yl)ureido)-3-Oxohexyl)-N-Hydroxyformamide
 UHF
 zinc ion
 ZN
 N-((2r,4s)-2-Butyl-4-(3-(2-Fluorophenyl)ureido)-5-Methyl-3-Oxohexyl)-N-Hydroxyformamide
 zinc ion
 ZN
 (S)-N-(Cyclopentylmethyl)-2-(3-(3,5-Difluorophenyl)ureido)-N-(2-(Hydroxyamino)-2-Oxoethyl)-3,3-Dimethylbutanamide
 UDB
 zinc ion
 ZN
 zinc ion
 ZN
 (S)-N-(Cyclopentylmethyl)-N-(2-(Hydroxyamino)-2-Oxoethyl)-2-(3-(2-Methoxyphenyl)ureido)-3,3-Dimethylbutanamide
 MDB
 Phosphate ion
 PO4
 1,2-ethanediol
 EDO
 protoporphyrin ix containing fe
 Hem
 36F
 5-Methyl-3-[4-(4-{5-[(3as,4s,6ar)-2-Oxohexahydro-1h-Thieno[3,4-D]imidazol-4-Yl]pentyl}-1h-1,2,3-Triazol-1-Yl)butyl]-1,3-Benzoxazol-2(3h)-One
 glycerol
 GOL
 BTX
 magnesium ion
 MG
 [(2r,3r,4r,5s,6r)-4-[(2r)-1-[[(2s)-1-[2-[2-[2-[5-[(3as,4s,6ar)-2-Oxidanylidene-1,3,3a,4,6,6a-Hexahydrothieno[3,4-D]imidazol-4-Yl]pentanoylamino]ethoxy]ethoxy]ethylamino]-1-Oxidanylidene-Propan-2-Yl]amino]-1-Oxidanylidene-Propan-2-Yl]oxy-3-Acetamido-5-[(2s,3r,4r,5r,6r)-3-Acetamido-6-(Hydroxymethyl)-4,5-Bis(Oxidanyl)oxan-2-Yl]oxy-6-(Hydroxymethyl)oxan-2-Yl] [oxidanyl(3,7,11,15,19,23,27,31,35,39,43-Undecamethyltetratetraconta-2,6,10,14,18,22,26,30,34,38,42-Undecaenoxy)phosphoryl]hydrogen Phosphate
 LHI
 Moenomycin
 M0E
 magnesium ion
 MG
 Acetate Ion
 ACT
 INS
 Isopropyl alcohol
 ipa
 Sulfate ion
 SO4
 magnesium ion
 MG
 TRS
 biotin
 BTN
 BIOTINYL-5-AMP
 BT5
 chloride ion
 CL
 calcium ion
 CA
 calcium ion
 CA
 3'-azido-3'-deoxythymidine-5'-monophosphate
 ATM
 Zaragozic acid A
 ZGA
 L(+)-Tartaric acid
 TLA
 Sulfate ion
 SO4
 chloride ion
 CL
 magnesium ion
 MG
 chloride ion
 CL
 1,2-ethanediol
 EDO
 TRS
 2'-Deoxyuridine 5'-monophosphate
 UMP
 Toxin A (Staphylococcus aureus clone pETA3 exfoliative)
 112024-90-5
 EINECS 297-504-8
 Toxins, entero-, A, Staphylococcus aureus
 93572-74-8
 Tefibazumab
 Aurexis
 INH-H 2002 
 Leukocidin F (Staphylococcus aureus strain ATCC49775 gene lukF-PV)
 171237-56-2
 EINECS 259-611-8
 Proteinase, Staphylococcus aureus serine
 55354-32-0
 Lysostaphin
 9011-93-2
 Qaca protein
 Efflux protein A
 Efpa gene product 
 NIOSH/MH9200000
 MH92000000
 delta-Hemolysin (Staphylococcus aureus wood 46 strain)
 Vga protein
 Vga gene product
 Vgab gene product 
 Lukf protein component
 Leukocidin F-component protein
 Leukocidin (Staphylococcus aureus clone pFRK92 gene lukF component F precursor) 
 Toxin B, entero-, (Staphylococcus reduced)
 11100-45-1
 12585-40-9
 Staphylococcus albus, dried
 NIOSH/WI0195800
 WI01958000
 Staphylococcus aureus
 Staphylococcus aureus (Micrococcaceae)
 68583-38-0
 Staphylococcus aureus, lysate
 EINECS 295-870-3
 92129-70-9
 Nora protein, staph
 Nora protein, staphylococcus
 Protein (Saphylococcus aureus clone pTUS829 gene norA) 
 EINECS 297-260-2
 Toxins, entero-, D, Staphylococcus aureus
 93384-49-7
 Leucocidin
 Leukocidin
 Pseudomonas aeruginos cytotoxin 
 Toxins, entero-, A
 Staphylococcal enterotoxin A
 Enterotoxin A, staphylococcal 
 Enterotoxin B (Staphylococcus aureus) 239
 210293-63-3
 EDIN
 Epidermal cell differentiation inhibitor
 Protein EDIN (Staphylococcus aureus strain E-1) 
 EINECS 297-261-8
 Toxins, entero-, E, Staphylococcus aureus
 93384-50-0
 TSST-1
 Staphylococcal Enterotoxin F
 NIOSH/KA8083000 
 Proteins, A, Staphylococcus aureus
 EINECS 297-246-6
 93384-36-2
 Toxin-1
 Toxin-1, toxic shock syndrome
 Toxin 1 (Staphylococcus aureus clone pRN6100 toxic shock) 
 Staphyloimmunocide
 2,4-Imidazolidinedione, 1-((3-(5-nitro-2-furanyl)-2-propenylidene)amino)-, potassium salt, mixt. with 8-quinolinol sulfate (2:1) (salt) and staphylococcus aureus
 79102-49-1
 Leukocidin S (Staphylococcus aureus strain ATCC49775 gene lukS-PV)
 171237-55-1
 Staphylococcus exotoxin
 Exotoxin, staphylococcus
 NIOSH/LF3060000 
 Staphylokinase
 auR protein, Staphylococcus aureus
 Kinase(enzyme-activating), staphylo 
 Toxoids, Staphylococcus aureus
 EINECS 308-524-4
 98072-69-6
 Heptocoagulase
 Peptocoagulase
 Plasmocoagulase 
 EINECS 297-505-3
 Toxins, entero-, B, Staphylococcus aureus
 93572-75-9
 Staphylokokkentoxin
 Staphylococcus toxin
 Staphylokokkentoxin [German] 
 delta-Hemolysin
 delta-Lysin
 Hld protein 
 Staph enterotoxin B
 Toxins, entero-, B
 Staphylococcal enterotoxin B 
 Protein A
 Staphylococcal Protein A
 CCRIS 4112 
 beta-Hemolysin (Staphylococcus aureus reduced)
 121890-76-4
 EINECS 297-259-7
 Toxins, entero-, C, Staphylococcus aureus
 93384-48-6
 Staphylococcus aureus lysozyme
 NIOSH/WI0195850
 WI01958500
 Staphylotoxin
 Staphylococcal alpha-toxin
 alpha-Staphylotoxin (9CI) 
 Minomycin
 Minocin
 Vectrin 
 minocycline
 Minocyclin
 Minociclina 
 MALONATE ION
 MLI
 thymidine-5'-phosphate
 TMP
 thymidine-5'-phosphate
 TMP
 thymidine-5'-phosphate
 TMP
 thymidine-5'-phosphate
 TMP
 6-Ethyl-5-[3-(4-Methoxybiphenyl-3-Yl)prop-1-Yn-1-Yl]pyrimidine-2,4-Diamine
 5DR
 nadp nicotinamide-adenine-dinucleotide phosphate
 NAP
 06W
 6-Ethyl-5-{(3s)-3-[3-Methoxy-5-(Pyridin-4-Yl)phenyl]but-1-Yn-1-Yl}pyrimidine-2,4-Diamine
 nadp nicotinamide-adenine-dinucleotide phosphate
 NAP
 Sulfate ion
 SO4
 3-METHYL-3H-PURIN-6-YLAMINE
 ADK
 zinc ion
 ZN
 Sulfate ion
 SO4
 zinc ion
 ZN
 formic acid
 FMT
 zinc ion
 ZN
 glycerol
 GOL
 zinc ion
 ZN
 chloride ion
 CL
 Cadmium ion
 CD
 minocycline
 Minocyclin
 Minociclina 
 glycerol
 GOL
 Sulfate ion
 SO4
 staph alpha-lysin
 Staph alpha-toxin-lysin
 Staphylococcal alpha-toxin 
 Protein A
 A, Protein
 Staphylococcal Protein A 
 staph enterotoxin A
 Staphylococcal enterotoxin A
 Enterotoxin A, staphylococcal 
 toxic-shock toxin
 Staph enterotoxin F
 TSST-1 
 TETRAETHYLENE GLYCOL
 PG4
 TETRAETHYLENE GLYCOL
 PG4
 NADPH DIHYDRO-NICOTINAMIDE-ADENINE-DINUCLEOTIDE PHOSPHATE
 NDP
 potassium ion
 S,R MESO-TARTARIC ACID
 SRT
 calcium ion
 CA
 Sulfate ion
 SO4
 Acetate Ion
 ACT
 Sulfate ion
 SO4
 Pyridoxal-5'-phosphate
 PLP
 chloride ion
 CL
 chloride ion
 CL
 GNF-PF-3839
 Phosphate ion
 PO4
 zinc ion
 ZN
 minocycline
 1371-EP2316450A1
 minocycline
 1371-EP2311799A1
 minocycline
 1371-EP2311798A1
 minocycline
 1371-EP2311797A1
 minocycline
 1371-EP2311796A1
 minocycline
 1371-EP2311451A1
 minocycline
 1371-EP2308863A1
 minocycline
 1371-EP2308832A1
 minocycline
 1371-EP2305637A2
 minocycline
 1371-EP2301916A2
 minocycline
 1371-EP2301914A1
 minocycline
 1371-EP2301913A1
 minocycline
 1371-EP2301912A2
 minocycline
 1371-EP2301534A1
 minocycline
 1371-EP2298732A1
 minocycline
 1371-EP2295427A1
 minocycline
 1371-EP2295426A1
 minocycline
 1371-EP2295419A2
 minocycline
 1371-EP2295402A2
 minocycline
 1371-EP2292612A2
 minocycline
 1371-EP2292590A2
 minocycline
 1371-EP2289871A1
 minocycline
 1371-EP2287150A2
 minocycline
 1371-EP2287148A2
 minocycline
 1371-EP2287140A2
 minocycline
 1371-EP2284164A2
 minocycline
 1371-EP2284156A2
 minocycline
 1371-EP2284155A2
 minocycline
 1371-EP2284153A2
 minocycline
 1371-EP2284152A2
 minocycline
 1371-EP2284151A2
 minocycline
 1371-EP2284150A2
 minocycline
 1371-EP2272832A1
 minocycline
 1371-EP2269991A2
 minocycline
 1371-EP2269985A2
 minocycline
 1371-EP2269978A2
 MINOCYCLINE HYDROCHLORIDE
 13614-98-7
 MINOCYCLINE HYDROCHLORIDE
 13614-98-7
 MINOCYCLINE HYDROCHLORIDE
 13614-98-7
 MINOCYCLINE HYDROCHLORIDE
 13614-98-7
 MINOCYCLINE HYDROCHLORIDE
 13614-98-7
 magnesium ion
 MG
 PEG
 glycerol
 GOL
 glycerol
 GOL
 TRIETHYLENE GLYCOL
 PGE
 PEG
 chloride ion
 CL
 TRS
 chloride ion
 CL
 TETRAETHYLENE GLYCOL
 PG4
 BTB
 glycerol
 GOL
 (2R,3S)-1,4-DIMERCAPTOBUTANE-2,3-DIOL
 DTU
 (2S,3S)-1,4-DIMERCAPTOBUTANE-2,3-DIOL
 DTV
 TETRAETHYLENE GLYCOL
 PG4
 Sulfate ion
 SO4
 (4S)-2-METHYL-2,4-PENTANEDIOL
 MPD
 protoporphyrin ix containing fe
 Hem
 protoporphyrin ix containing fe
 Hem
 glycerol
 GOL
 calcium ion
 CA
 Sulfate ion
 SO4
 Sulfate ion
 SO4
 glycerol
 GOL
 protoporphyrin ix containing co
 COH
 glycerol
 GOL
 protoporphyrin ix containing fe
 Hem
 protoporphyrin ix containing fe
 Hem
 glycerol
 GOL
 Sulfate ion
 SO4
 protoporphyrin ix containing fe
 Hem
 protoporphyrin ix containing fe
 Hem
 glycerol
 GOL
 Sulfate ion
 SO4
 protoporphyrin ix containing fe
 Hem
 glycerol
 GOL
 protoporphyrin ix containing fe
 Hem
 magnesium ion
 MG
 VANADATE ION
 VO4
 adenosine-5'-diphosphate
 ADP
 magnesium ion
 MG
 uridine-5'-diphosphate
 UDP
 adenosine-5'-diphosphate
 ADP
 magnesium ion
 MG
 adenosine-5'-diphosphate
 ADP
 cytidine-5'-diphosphate
 CDP
 magnesium ion
 MG
 magnesium ion
 MG
 Guanosine-5'-triphosphate
 GTP
 glycerol
 GOL
 Sulfate ion
 SO4
 (3r)-3-(Methylsulfanyl)-1-Azabicyclo[2.2.2]octane
 MHT
 DOL
 DOL
 magnesium ion
 MG
 
 
 acetoacetyl-coenzyme a
 CAA
 acetoacetyl-coenzyme a
 CAA
 3-hydroxy-3-methylglutaryl-coenzyme A
 HMG
 TRIETHYLENE GLYCOL
 PGE
 1,2-ethanediol
 EDO
 Sulfate ion
 SO4
 chloride ion
 CL
 calcium ion
 CA
 CARBONATE ION
 CO3
 Minomycin
 MINOCYCLINE HYDROCHLORIDE
 Minocycline chloride 
 minocycline
 Minocyclin
 Minociclina 
 Acetate Ion
 ACT
 glycerol
 GOL
 Sulfate ion
 SO4
 glycerol
 GOL
 (4r,5s)-3-{[(3s,5s)-5-(Dimethylcarbamoyl)pyrrolidin-3-Yl]sulfanyl}-5-[(2s,3r)-3-Hydroxy-1-Oxobutan-2-Yl]-4-Methyl-4,5-Dihydro-1h-Pyrrole-2-Carboxylic Acid
 MER
 glycerol
 GOL
 (5r)-5-[(1s,2r)-1-Formyl-2-Hydroxypropyl]-3-[(2-{[(E)-Iminomethyl]amino}ethyl)sulfanyl]-4,5-Dihydro-1h-Pyrrole-2-Carboxylic Acid
 IM2
 Sulfate ion
 SO4
 glycerol
 GOL
 pyrophosphate 2-
 POP
 adenosine-5'-diphosphate
 ADP
 magnesium ion
 MG
 calcium ion
 CA
 (2r,4s)-2-[(1r)-1-{[(2'-Carboxybiphenyl-2-Yl)carbonyl]amino}-2-Oxoethyl]-5,5-Dimethyl-1,3-Thiazolidine-4-Carboxylic Acid
 BOU
 Sulfate ion
 SO4
 MINOCYCLINE HYDROCHLORIDE
 CHEMBL1257129
 TETRAETHYLENE GLYCOL
 PG4
 TRIETHYLENE GLYCOL
 PGE
 TETRAETHYLENE GLYCOL
 PG4
 PEG
 PEG
 Isopropyl alcohol
 ipa
 citric acid
 cit
 calcium ion
 CA
 zinc ion
 ZN
 Mupirocin
 MRC
 zinc ion
 ZN
 magnesium ion
 MG
 potassium ion
 magnesium ion
 MG
 protoporphyrin ix containing fe
 Hem
 chloride ion
 CL
 protoporphyrin ix containing fe
 Hem
 magnesium ion
 MG
 protoporphyrin ix containing fe
 Hem
 chloride ion
 CL
 Sulfate ion
 SO4
 EPE
 NICKEL (II) ION
 NI
 histidine
 his
 glycerol
 GOL
 (3r)-3-(Fluoromethyl)-3-Hydroxy-5-{[(S)-Hydroxy(Phosphonooxy)phosphoryl]oxy}pentanoic Acid
 FM0
 (3r)-3-(Fluoromethyl)-3-Hydroxy-5-{[(S)-Hydroxy(Phosphonooxy)phosphoryl]oxy}pentanoic Acid
 FM0
 1-({[(S)-Hydroxy(Phosphonooxy)phosphoryl]oxy}acetyl)-L-Proline
 2P0
 chloride ion
 CL
 TRIETHYLENE GLYCOL
 PGE
 Minomycin
 FT-0082736
 citric acid
 cit
 sodium ion
 na
 glycerol
 GOL
 MNR
 glycerol
 GOL
 Sulfate ion
 SO4
 Protoporphyrin Ix Containing Ga
 GIX
 minocycline
 chloride ion
 CL
 magnesium ion
 MG
 FE (III) ION
 FE
 Sulfate ion
 SO4
 Nicotinamide-Adenine-Dinucleotide
 NAD
 minocycline
 CHEMBL1434
 chloride ion
 CL
 flavin mononucleotide
 FMN
 1,2-ethanediol
 EDO
 PEG
 2-oxoglutaric acid
 AKG
 MANGANESE (II) ION
 MN
 zinc ion
 ZN
 Sulfate ion
 SO4
 glycerol
 GOL
 1,2-ethanediol
 EDO
 sodium ion
 na
 sodium ion
 na
 chloride ion
 CL
 MINOCYCLINE HYDROCHLORIDE
 CPD001906766
 SAM002264621
 EUROPIUM ION
 EU
 N-acetyl-D-glucosamine
 NAG
 N-acetyl-D-glucosamine
 NAG
 MINOCYCLINE HYDROCHLORIDE
 TRIETHYLENE GLYCOL
 PGE
 MINOCYCLINE HYDROCHLORIDE
 minocycline
 CHEMBL1200881
 minocycline
 CHEMBL259172
 LL-37
 C18313
 sodium ion
 na
 glycerol
 GOL
 glycerol
 GOL
 magnesium ion
 MG
 GGS
 Phosphonooxy-[(10e)-3,7,11,15-Tetramethylhexadeca-2,6,10,14-Tetraenyl]sulfanyl-Phosphinic Acid
 magnesium ion
 MG
 {(1r,2r,3r)-2-[(3e)-4,8-Dimethylnona-3,7-Dien-1-Yl]-2-Methyl-3-[(1e,5e)-2,6,10-Trimethylundeca-1,5,9-Trien-1-Yl]cyclopropyl}methyl Trihydrogen Diphosphate
 PS7
 magnesium ion
 MG
 (1r)-4-[3-(2-Benzylphenoxy)phenyl]-1-Phosphonobutane-1-Sulfonic Acid
 N-(1-Methylethyl)-3-[(3-Prop-2-En-1-Ylbiphenyl-4-Yl)oxy]propan-1-Amine
 (3r)-3-Biphenyl-4-Yl-1-Azabicyclo[2.2.2]octan-3-Ol
 EDO
 gly
 cit
 Arbekacina
 Arbekacine
 Arbekacinum 
 zinc ion
 ZN
 EDO
 CL
 SO4
 ANP
 PO4
 BME
 na
 PO4
 MG
 PEG
 CAC
 Sal
 glycerol
 GOL
 GOL
 glycerol
 GOL
 potassium ion
 glycerol
 GOL
 5-[(2-{[(3s)-5-{[(2s)-2-Amino-2-Carboxyethyl]amino}-3-Carboxy-3-Hydroxy-5-Oxopentanoyl]amino}ethyl)amino]-2,5-Dioxopentanoic Acid
 Se8
 FE (III) ION
 FE
 1,2-ethanediol
 EDO
 chloride ion
 CL
 MPD
 MRD
 SO4
 CL
 NAD
 GOL
 GOL
 CL
 G3H
 GOL
 NAD
 NAD
 3PG
 3-phosphoglyceric acid
 3PG
 Nicotinamide-Adenine-Dinucleotide
 NAD
 3-phosphoglyceric acid
 3PG
 3-phosphoglyceric acid
 3PG
 Nicotinamide-Adenine-Dinucleotide
 NAD
 Nicotinamide-Adenine-Dinucleotide
 NAD
 Nicotinamide-Adenine-Dinucleotide
 NAD
 CL
 GOL
 NAD
 DIACYL GLYCEROL
 DGA
 6-Methoxy-4-(2-{4-[([1,3]oxathiolo[5,4-C]pyridin-6-Ylmethyl)amino]piperidin-1-Yl}ethyl)quinoline-3-Carbonitrile
 RXV
 CA
 SO4
 MRD
 MPD
 ACT
 IMD
 52V
 NAP
 PG4
 ACY
 PAJ
 CL
 cit
 MG
 PO4
 EPE
 PLP
 ACT
 ZN
 PO4
 EDO
 SM
 SO4
 APC
 GOL
 NAP
 RAR
 GOL
 NAP
 RAR
 PO4
 MG
 chloride ion
 CL
 calcium ion
 CA
 chloride ion
 CL
 potassium ion
 Phosphate ion
 PO4
 MANGANESE (II) ION
 MN
 Nicotinamide-Adenine-Dinucleotide
 NAD
 SO4
 ACT
 CA
 KANAMYCIN A
 KAN
 MII
 AIC
 CAC
 PNN
 chloride ion
 CL
 SO4
 ipa
 SO4
 SME
 PEG
 MINOCYCLINE HYDROCHLORIDE
 AC-14547
 GOL
 PYR
 NAD
 MINOCYCLINE HYDROCHLORIDE
 M 9511
 CL
 BU1
 ZN
 IMD
 B3P
 zinc ion
 ZN
 PEG
 1,2-ethanediol
 EDO
 TETRAETHYLENE GLYCOL
 PG4
 chloride ion
 CL
 HEXAETHYLENE GLYCOL
 P6G
 Adenosine monophosphate
 AMP
 Oxalate Ion
 OXL
 Minomycin
 Minocin
 MINOCYCLINE HYDROCHLORIDE 
 na
 NAP
 N22
 B47
 B48
 zinc ion
 ZN
 Phosphate ion
 PO4
 sodium ion
 na
 chloride ion
 CL
 I2H
 NAP
 SO4
 TA6
 ANP
 MG
 ZZ7
 CEW
 MLS002222327
 MINOCYCLINE HYDROCHLORIDE
 SMR001307266
 glycerol
 GOL
 zinc ion
 ZN
 Sulfate ion
 SO4
 NAP
 TCL
 NAP
 na
 53T
 NAP
 52V
 NAP
 53T
 NAP
 52V
 NAP
 Beta-Mercaptoethanol
 BME
 TRS
 MXE
 sodium ion
 na
 NICKEL (II) ION
 NI
 chloride ion
 CL
 
 palmitic acid
 PLM
 chloride ion
 CL
 NICKEL (II) ION
 NI
 NICKEL (II) ION
 NI
 palmitic acid
 PLM
 chloride ion
 CL
 minocycline
 13614-98-7
 glycerol
 GOL
 zinc ion
 ZN
 glycerol
 GOL
 Acetate Ion
 ACT
 Benzenethiol
 BT6
 Unknown Ligand
 UNL
 Unknown Ligand
 UNL
 Unknown Ligand
 UNL
 2-[N-CYCLOHEXYLAMINO]ETHANE SULFONIC ACID
 NHE
 MN
 N-acetyl-D-glucosamine
 NAG
 adenosine-5'-diphosphate
 ADP
 2-(N-MORPHOLINO)-ETHANESULFONIC ACID
 MES
 N-acetyl-D-glucosamine
 NAG
 N-acetyl-D-glucosamine
 NAG
 Minomycin
 CID6602603
 Minomycin
 CID5281093
 Minomycin
 CID5362501
 minocycline
 CID6604341
 minocycline
 CID5281021
 minocycline
 CID5388969
 minocycline
 CID9804139
 minocycline
 CID4200
 minocycline
 CID5362502
 minocycline
 CID5353779
 minocycline
 CID11396950
 dimethyl sulfoxide
 DMS
 glycerol
 GOL
 sodium ion
 na
 chloride ion
 CL
 bromide ion
 BR
 zinc ion
 ZN
 minocycline
 LMPK07000002
 PYR
 NAD
 Sulfate ion
 SO4
 thymidine
 THM
 chloride ion
 CL
 chloride ion
 CL
 glycerol
 GOL
 calcium ion
 CA
 chloride ion
 CL
 zinc ion
 ZN
 NH2
 ACE
 magnesium ion
 MG
 3,6,9,12,15,18-Hexaoxaicosane-1,20-diol
 P33
 Unknown Ligand
 UNL
 SKM
 glycerol
 GOL
 PO4
 na
 glycerol
 GOL
 zinc ion
 ZN
 chloride ion
 CL
 Phosphate ion
 PO4
 glycerol
 GOL
 adenosine-5'-diphosphate
 ADP
 NAP
 11F
 11F
 NAP
 N22
 NAP
 NAP
 55V
 55V
 NAP
 N22
 NAP
 53R
 NAP
 53R
 NAP
 FE
 CD
 trimethoprim
 Top
 NADPH DIHYDRO-NICOTINAMIDE-ADENINE-DINUCLEOTIDE PHOSPHATE
 NDP
 glycerol
 GOL
 trimethoprim
 Top
 1,2-ethanediol
 EDO
 NADPH DIHYDRO-NICOTINAMIDE-ADENINE-DINUCLEOTIDE PHOSPHATE
 NDP
 trimethoprim
 Top
 PEG
 glycerol
 GOL
 TETRAETHYLENE GLYCOL
 PG4
 acetic acid
 ACY
 zinc ion
 ZN
 sodium ion
 na
 formic acid
 FMT
 chloride ion
 CL
 calcium ion
 CA
 TRIETHYLENE GLYCOL
 PGE
 glycerol
 GOL
 Unknown Ligand
 UNL
 flavin mononucleotide
 FMN
 CITRATE ANION
 FLC
 2-[N-CYCLOHEXYLAMINO]ETHANE SULFONIC ACID
 NHE
 Phosphate ion
 PO4
 sodium ion
 na
 glycerol
 GOL
 SO4
 glucose-6-phosphate
 G6Q
 sodium ion
 na
 GP9
 ACT
 MN
 GP9
 ACT
 MN
 GP9
 MN
 ACT
 EDO
 MN
 MES
 2-(N-MORPHOLINO)-ETHANESULFONIC ACID
 Sulfate ion
 SO4
 sodium ion
 na
 MINOCYCLINE HYDROCHLORIDE
 MLS002153492
 SMR000326784
 na
 sodium ion
 na
 Nicotinamide-Adenine-Dinucleotide
 NAD
 Beta-Mercaptoethanol
 BME
 magnesium ion
 MG
 1,2-ethanediol
 EDO
 Acetate Ion
 ACT
 calcium ion
 CA
 Minomycin
 Arestin
 Dynacin 
 minocycline
 Minociclina
 minociclinum 
 SEB superantigen
 Staph enterotoxin B
 Staphylococcal enterotoxin B 
 minocycline
 Minocin
 Dynacin 
 Beta-Mercaptoethanol
 BME
 magnesium ion
 MG
 glycerol
 GOL
 magnesium ion
 MG
 Pyruvic acid
 PYR
 glycerol
 GOL
 MINOCYCLINE HYDROCHLORIDE
 NCGC00162259-01
 MINOCYCLINE HYDROCHLORIDE
 NCGC00094135-01
 magnesium ion
 MG
 Sulfate ion
 SO4
 chloride ion
 CL
 glycerol
 GOL
 NH2
 ACE
 AMINO GROUP
 NH2
 ACETYL GROUP
 ACE
 succinic acid
 SIN
 potassium ion
 Thiocyanate ion
 SCN
 Hem
 Staphylotoxin
 alpha-Staphylotoxin
 Staphylococcal alpha-toxin 
 Minomycin
 Minocin
 MINOCYCLINE HYDROCHLORIDE 
 minocycline
 Minocyclin
 Minocin 
 Minomycin
 MINOCYCLINE HYDROCHLORIDE
 LS-91518
 Lysostaphin
 Lysostaphin [USAN]
 LS-88505
 Leucocidin
 Leukocidin
 Pseudomonas aeruginos cytotoxin 
 Toxins, entero-, A
 Staphylococcal enterotoxin A
 Enterotoxin A, staphylococcal 
 Heptocoagulase
 Peptocoagulase
 Plasmocoagulase 
 FMC
 Unknown Ligand
 UNL
 Flavin-adenine dinucleotide
 FAD
 Sulfate ion
 SO4
 1,4-BUTANEDIOL
 BU1
 acetic acid
 ACY
 MG
 Tripotassium (1r)-4-Biphenyl-4-Yl-1-Phosphonatobutane-1-Sulfonate
 B70
 MG
 B69
 MG
 B65
 ATP
 PYR
 BTI
 MN
 (2r,4ar,5ar,11ar,12as)-8-Amino-2-Hydroxy-4a,5a,9,11,11a,12a-Hexahydro[1,3,2]dioxaphosphinino[4',5':5,6]pyrano[3,2-G]pteridine-10,12(4h,6h)-Dione 2-Oxide
 8CS
 glycerol
 GOL
 Aurexis (TN)
 Tefibazumab (USAN/INN)
 D06054 
 MINO
 Minocycline (USAN/INN)
 D05045 
 Defensin beta-4
 C16062
 Defensin beta-3
 C16061
 Defensin beta-2
 C16060
 Defensin beta-1
 C16059
 Defensin alpha-5
 C16058
 Defensin alpha-4
 C16057
 Defensin alpha-3
 C16056
 Defensin alpha-2
 C16055
 Defensin alpha-1
 C16054
 glycerol
 GOL
 sodium ion
 na
 guanosine-5'-monophosphate
 5GP
 potassium ion
 Sulfate ion
 SO4
 chloride ion
 CL
 magnesium ion
 MG
 glycerol
 GOL
 Acetate Ion
 ACT
 Sulfate ion
 SO4
 Altabax (Glaxo)
 DB01256
 SB 275833
 Tygacil
 GAR-936,Tigecycline
 WAY-GAR-936 
 Meticillin
 Methicillinum
 Methycillin 
 Erythromycin A
 Erymax
 erythro 
 Maxipime
 Axepim
 Cepimax 
 adenylic acid
 5'-adenylic acid
 Phosphaden 
 Amoxycillin
 Clamoxyl
 Amopenixin 
 Penetrex
 DB00467
 Cefalexin
 Cephacillin
 Ceporexine 
 Garamycin
 Alcomicin
 Bristagen 
 Ertapenem
 Invanz
 DB00303 
 Zagam
 DB01208
 NONANEDIOIC ACID
 Finacea
 Azelex 
 LEVOFLOXACIN
 Levaquin
 Quixin 
 meropenem
 Merrem
 Meropenem anhydrous 
 minocycline
 Klinomycin
 Minocyclin 
 MINOCYCLINE HYDROCHLORIDE
 MLS000859923
 SMR000326784
 glycerol
 GOL
 Sulfate ion
 SO4
 acetoacetyl-coenzyme a
 CAA
 zinc ion
 ZN
 Sulfate ion
 SO4
 1-{3-[(4-Pyridin-2-Ylpiperazin-1-Yl)sulfonyl]phenyl}-3-(1,3-Thiazol-2-Yl)urea
 GAX
 1-{3-[(4-Pyridin-2-Ylpiperazin-1-Yl)sulfonyl]phenyl}-3-(1,3-Thiazol-2-Yl)urea
 GAX
 zinc ion
 ZN
 sodium ion
 na
 1,2-ethanediol
 EDO
 chloride ion
 CL
 glycerol
 GOL
 calcium ion
 CA
 N-acetyl-D-glucosamine
 NAG
 1,2-ethanediol
 EDO
 Sulfate ion
 SO4
 PEG
 1,2-ethanediol
 EDO
 Acetate Ion
 ACT
 sodium ion
 na
 PH2
 zinc ion
 ZN
 MES
 2-(N-MORPHOLINO)-ETHANESULFONIC ACID
 chloride ion
 CL
 E64
 ACT
 CO3
 SO4
 HH2
 
 MN
 TRS
 zinc ion
 ZN
 Sulfate ion
 SO4
 Sulfate ion
 SO4
 D-glutamic acid
 DGL
 chloride ion
 CL
 chloride ion
 CL
 Phosphate ion
 PO4
 calcium ion
 CA
 zinc ion
 ZN
 adenosine-5'-diphosphate
 ADP
 sodium ion
 na
 glycerol
 GOL
 Sulfate ion
 SO4
 MINOCYCLINE HYDROCHLORIDE
 M9511_SIGMA
 zinc ion
 ZN
 zinc ion
 ZN
 potassium ion
 Pyridoxal-5'-phosphate
 PLP
 1,2-ethanediol
 EDO
 Sulfate ion
 SO4
 protoporphyrin ix containing fe
 Hem
 2-[N-CYCLOHEXYLAMINO]ETHANE SULFONIC ACID
 NHE
 calcium ion
 CA
 calcium ion
 CA
 ACETYL COENZYME A
 ACO
 zinc ion
 ZN
 sodium ion
 na
 1,2-ethanediol
 EDO
 glycerol
 GOL
 minocycline
 MIY
 1,2-ethanediol
 EDO
 adenosine-5'-diphosphate
 ADP
 magnesium ion
 MG
 Sulfate ion
 SO4
 Sulfate ion
 SO4
 3-chloro-2,2-dimethyl-N-[4-(trifluoromethyl)phenyl]propanamide
 G1L
 Sulfate ion
 SO4
 SO4
 Sulfate ion
 SO4
 Nicotinic acid adenine dinucleotide
 DND
 calcium ion
 CA
 Nicotinic acid adenine dinucleotide
 DND
 PARAQUAT
 5,5'-Dithiobis(2-nitrobenzoic acid)
 4-Chloro-7-nitrobenzo-2-oxa-1,3-diazole
 CUMENE HYDROPEROXIDE
 VANCOMYCIN
 norfloxacin
 ciprofloxacin
 NICKEL (II) ION
 NI
 magnesium ion
 MG
 minocycline
 2-Naphthacenecarboxamide, 4,7-bis(dimethylamino)-1,4,4a,5,5a,6,11,12a-octahydro-3,10,12,12a-tetrahydroxy-1,11-dioxo-
 MINOCYCLINE HYDROCHLORIDE
 Prestwick_626
 13614-98-7
 CA
 MINOCYCLINE HYDROCHLORIDE
 chloride ion
 CL
 Phosphate ion
 PO4
 ZN
 Unknown Ligand
 UNL
 Sulfate ion
 SO4
 magnesium ion
 MG
 zinc ion
 ZN
 Phosphate ion
 PO4
 Phosphate ion
 PO4
 zinc ion
 ZN
 zinc ion
 ZN
 Sulfate ion
 SO4
 Sulfate ion
 SO4
 Sulfate ion
 SO4
 Phosphate ion
 PO4
 CL
 zinc ion
 ZN
 Cadmium ion
 CD
 zinc ion
 ZN
 Sulfate ion
 SO4
 Sulfate ion
 SO4
 zinc ion
 ZN
 zinc ion
 ZN
 calcium ion
 CA
 potassium ion
 zinc ion
 ZN
 zinc ion
 ZN
 zinc ion
 ZN
 magnesium ion
 MG
 (2r,4s)-2-[(1r)-1-{[(2,6-Dimethoxyphenyl)carbonyl]amino}-2-Oxoethyl]-5,5-Dimethyl-1,3-Thiazolidine-4-Carboxylic Acid
 7EP
 Cadmium ion
 CD
 Cadmium ion
 CD
 Cadmium ion
 CD
 glycerol
 GOL
 FE (III) ION
 FE
 SO4
 CED
 Sulfate ion
 SO4
 CA
 ZN
 
 minocycline
 MLS000028367
 MINOCYCLINE HYDROCHLORIDE
 SMR000058620
 beta-D-galactose
 GAL
 alpha-D-Mannose
 MAN

 FUC
 N-acetyl-D-glucosamine
 NAG
 beta-D-galactose
 GAL
 alpha-D-Mannose
 MAN
 
 FUC
 N-acetyl-D-glucosamine
 NAG
 phosphocholine
 PC
 actinonin
 BB2
 methionine
 met
 
 threonine
 thr
 Nicotinamide-Adenine-Dinucleotide
 NAD
 ETHYLISOTHIOUREA
 ITU
 sucrose
 suc
 [2-Amino-3-(4-Hydroxy-Phenyl)-Propionylamino]- (2,4,5,8-Tetrahydroxy-7-Oxa-2-Aza-Bicyclo[3.2.1]oct-3-Yl)-Acetic Acid
 glc
 PNM
 Sulfate ion
 SO4
 minocycline
 Minocline
 Minocin (Hydrochloride) 
 3-hydroxy-3-methylglutaryl-coenzyme A
 HMG
 acetoacetyl-coenzyme a
 CAA
 3-dehydroshikimate
 DHK
 Minomycin
 Minocin
 Minomax 
 Nuclease, of Staphylococcus aureus
 NSC260544
 NSC-260544
 FMLP
 N-Formyl-L-methionyl-L-leucyl-L-phenylalanine
 C11596 
 minocycline
 C07225
 10118-90-8
 Lipoteichoic acid
 C06042
 (sn-Gro-1-P)25->6Glc-alpha1->2Glc-alpha1->3acyl2Gro
