1. Search for compounds in PubChem Substances, "streptomyces"
2. Save in file as ID Map or synomyms
3. Remove SID/CID lines: $ sed -i_bak '/SID/d' pcsubstance_result.txt
4. Remove line numbering: $ sed -Ei_bak2 's/^ *[0-9]*.//g' pcsubstance_result.txt
5. Remove ... : $ sed -Ei_bak3 's/\.\.\.//g' pcsubstance_result.txt
	=> CSV file with up to 3 entries/ compound
6. Remove empty lines: $ sed -i_bak4 '/^$/d' pcsubstance_result.txt
7. Line breaks instead of semicolon: $ sed -i_bak5 's/;/\n/g' pcsubstance_result.txt
8. Remove lines containing "OXY": $ sed -i_bak '/OXY/d' pcsubstance_result.txt
