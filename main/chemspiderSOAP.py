#    SpiderMass is a software for the automated generation of chemical databases and the identification of compounds.

#    Copyright (C) 2013-2017 Dr. Robert Winkler
#    email: robert.winkler@cinvestav.mx, robert.winkler@bioprocess.org
#    CINVESTAV Unidad Irapuato
#    Km. 9.6 Libramiento Norte Carr. Irapuato-León
#    36821 Irapuato Gto., México
#    Tel.: +52-462-6239-635
#    http://www.ira.cinvestav.mx/lababi.aspx

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# FOR USING THIS LIBRARY YOU HAVE TO REGISTER AT CHEMSPIDER
# (FREE FOR ACADEMICS) AND PUT THE TOLKEN IN A FILE CALLED
# chemspider-token.txt in the main directory of SpiderMass

import time

import numpy
import osa

CSclient = osa.Client("http://www.chemspider.com/Search.asmx?wsdl")

try:
    tokenfile = open("chemspider-token.txt", "r")
    ChemSpiderToken = tokenfile.readline()
    tokenfile.close()

    TOKEN = ChemSpiderToken

except:
    print("chemspiderSOAP error: No chemspider-token.txt file found.")
    print(
        "Please go to http://www.chemspider.com/ and register for optaining a free token."
    )
    print(
        "Place the token in a text file called chemspider-token.txt in the main directory."
    )
    print("ONLINE SOAP REQUESTS TO CHEMSPIDER WILL NOT WORK WITHOUT TOKEN.\n")

    TOKEN = "0"


def IntrinsicPropSeachMonoMWSOAP(MinMonoW, MaxMonoW, SearchTimeOut):

    # print(CSclient)
    # print(CSclient.types)
    # print(CSclient.service)

    # optionsString='EmpiricalFormula=C6H12O6'
    # commonOptionsString='Complexity=Any,Isotopic=Any,HasSpectra=false,HasPatents=false'

    # print(CSclient.service.IntrinsicPropertiesSearch.__doc__)
    # print(CSclient.service.GetAsyncSearchResult.__doc__)

    print(
        "Searching for Monoisotopic Mass ",
        MinMonoW,
        "..",
        MaxMonoW,
        " in ChemSpider.com Online",
    )

    opt = CSclient.types.IntrinsicPropertiesSearchOptions()
    comopt = CSclient.types.CommonSearchOptions()

    opt.EmpiricalFormula = ""
    opt.MolWeightMin = 0
    opt.MolWeightMax = 2000
    opt.NominalMassMin = 0
    opt.NominalMassMax = 2000
    opt.AverageMassMin = 0
    opt.AverageMassMax = 2000
    opt.MonoisotopicMassMin = MinMonoW
    opt.MonoisotopicMassMax = MaxMonoW

    comopt.Complexity = "Any"
    comopt.Isotopic = "Any"
    comopt.HasSpectra = "false"
    comopt.HasPatents = "false"

    # print(opt)
    # print(comopt)

    CSResultString = CSclient.service.IntrinsicPropertiesSearch(
        options=opt, commonOptions=comopt, token=TOKEN
    )

    SearchTimeOut = SearchTimeOut + 1

    for i in range(1, SearchTimeOut):

        CSSearchStatus = CSclient.service.GetAsyncSearchStatus(
            rid=CSResultString, token=TOKEN
        )
        print("Status of ChemSpider SOAP request: ", CSSearchStatus)

        if CSSearchStatus == "Processing":
            time.sleep(1)

        if CSSearchStatus == "ResultReady":
            resultCS = CSclient.service.GetAsyncSearchResult(
                rid=CSResultString, token=TOKEN
            )
            resultCSint = resultCS.int
            print("Obtaining results..")
            break

        if i == (SearchTimeOut - 1):
            resultCSint = ["empty"]
            print("Timeout: >", (SearchTimeOut - 1), "s without result")
            break

        if CSSearchStatus not in ("ResultReady", "Processing"):
            resultCSint = ["empty"]
            break

    print("SOAP request finished")

    return resultCSint
