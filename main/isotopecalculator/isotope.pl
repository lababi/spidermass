#!/usr/bin/perl -w

# isotope.pl, a perl cgi script providing a web interface to 'isotope'.
#
# -----------------------------------------------------------------
# Copyright (c) Joerg Hau <joerg.hau at dplanet.ch>.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 2 of the GNU General Public
# License as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# -----------------------------------------------------------------
# Revisions:
#   2005-06-17, first operational version (JHa)
# -----------------------------------------------------------------

use strict;
use CGI::Carp qw(fatalsToBrowser);

# send HTML header
#
print "Content-type: text/html\n\n";
print <<"ENDE";
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head><title>Calculated Isotope Pattern</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="author" content="Joerg Hau">
<link href="../style01.css" rel="stylesheet" type="text/css">
<script type="text/javascript" language="JavaScript">
    <!--
     if(top.frames.length > 0)
      top.location.href=self.location;
    //-->
</script>
</head>
ENDE

# split and parse parameter stream
#
read(STDIN, my $data, $ENV{'CONTENT_LENGTH'});
my @cgiPairs = split(/&/, $data);      # parse arguments into list of name-value pairs
my $pair;
my %cgiVals;                           # entire (associative) array
foreach $pair (@cgiPairs) {
  (my $name, my $val) = split(/=/, $pair);
  $val =~ tr/+/ /;
  $val =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
  $val =~ s/</&lt;/g;
  $val =~ s/>/&gt;/g;
  $cgiVals{"$name"} = "$val";
 }

# Extract all parameters and assign them to variables
#
my $frac = ($cgiVals{"fractional"} > 0) ? "-f" : "";
my $form = $cgiVals{"formula"};

# TODO: Run some error checks (numeric variables, excessive range, ...)

# do calculation, fetch output as '$out'
#
my $out = `./isotope $frac $form`;

# print as HTML
#
print "<body><h1>Calculated isotope pattern for $form</h1>\n";
print "<hr>\n<pre>\n";
print $out;
print "</pre>";

# print a "back" button
#
print "<form>\n<input type=\"button\" value=\"Back\" onClick=\"history.back()\">\n</form>\n";
print "<hr>\n";

# print program version as page footer
#
$out = `./isotope -v`;
print "<i>Powered by</i> ", $out;

print "</body></html>\n";
