README to ISOTOPE, 20050621
===========================

NAME

isotope - calculates isotope pattern for a given chemical formula


SYNOPSIS

    isotope [-f] [-v] [-h] [formula] [ < infile ] [ > outfile ]


DESCRIPTION

'isotope' calculates the isotope pattern for a given chemical formula.

If you are not working with mass spectrometry, if you are neither
chemist nor physicist, then this program is probably not for you ;-)

In contrast to most programs that I came across, this one is not an
application with a graphical interface, but but a small stand-alone
utility. It allows to run  calculations in batch mode, via the command
line, or via a web interface.

To run the program interactively, enter formulae when asked. Element
symbols must be correctly typed, with upper and lower case as usual.
Elements can be repeated in a formula, but brackets are not understood:
CH3OH is ok, but Ni(CO)4 is not: this has to be typed as NiC4O4.


OPTIONS

    -h      shows a short Help screen, then exit.
    -v      show Version information, then exit.
    -f      print fractional fractional intensities (default: scaled to 100%)
    formula Chemical formula to be calculated, e.g. 'C12H11O11' (see below)

    Note: All keycodes are case sensitive.


EXIT CODE

Exit code is
    0 if program execution was successful,
    1 if any error occurred.


BATCH MODE

You can redirect the output to a file or printer:

    isotope > filename

Calculation of a series of spectra can be done in batch mode.
Create an input file of all the formula, one per line, with a 'q' (quit)
as the first character of the last line. Then launch the program:

    isotope < infile > outfile.


EXAMPLES

isotope -h
    displays the help text, then exits.

isotope -v
    displays version information, then exits.

isotope
    launches the program in interactive mode. Enter <q> to exit.

isotope -f
    Sets display of calculation results to fractional intensities,
    then runs the program in interactive mode as above.

isotope C12H22O11
    runs the calculation for "C12H22O11", displays the result, then exits.

isotope -f < data.txt > results.txt
    Sets display of calculation results to fractional intensities,
    reads (multiple) formula from file 'data.txt'.
    and writes results to the file 'results.txt'.


LICENSE

This program and its documentation are Copyright (c) 1996-2005 by Joerg Hau.

This program is free software; you can redistribute it and/or modify it
under the terms of version 2 of the GNU General Public License ("GPL") as
published by the Free Software Foundation. See the file LICENSE for details.

If you use this program (or any part of it) in another application, note
that the resulting application becomes also GPL. In other words, GPL is a
"contaminating" license.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License (file
LICENSE) for more details.


OBTAINING THE PROGRAM

The latest version can be obtained via http from the author's website, in
the "Mass Spectrometry" section:

    http://homepage.sunrise.ch/mysunrise/joerg.hau/


FILES

The package you downloaded is a .tar.gz file, which can be handled by most
common "packagers" on most platforms. It contains the following files:

    README          The file you are just reading.
    LICENSE         The license (GPL).

    isotope         The executable for Linux (stripped binary).
    isotope.exe     The executable for Microsoft OS.
    isotope.c       The source code.

    isotope.html    Page to use 'hr' with a web server.
    isotope.pl      Perl script for using 'hr' with a web server.

To install, just copy the corresponding executable file to any location
you desire.

Invoke it by typing its name. As the program is a command-line utility,
it needs a terminal window. The executable for MS-DOS runs in a "DOS window"
under MS-Windows.

If you want/need to recompile: As this is a single file, I did not provide a
Makefile. This is ANSI C and should compile on any platform. Make sure to
optimize for speed as far as you can. Under Unix/Linux with gcc installed,
use something like

    gcc -O3 -Wall -o isotope isotope.c

The .exe file included here was generated with gcc under MinGW.


WEB USE

To use this software with a webserver, copy both the executable and the perl
script 'isotope.pl' in the webserver's cgi-bin directory.

Copy the HTML page 'isotope.html' in the html document tree. This page contains
a simple form that calls 'hr.pl' upon submission. The latter is a Perl script
that reads the parameters, executes hr, and displays the results via HTTP.

If you put this on a webpage, please keep the copyright note intact.


ALGORITHM

Basically, the program parses a given formula and generates the brute formula
first. Looping over all atoms of a given element, the isotope pattern is then
calculated by summing up all the abundances of all individual isotopes of an
element. The same calculation is then in turn performed the for all elements
in the formula. At each cycle, the mass and abundance scales are adjusted.

This algorithm uses nominal masses, and the result is obviously ;-) also
limited to nominal masses. While it is maybe not as accurate as more elaborate
algorithms, it nevertheless provides a fast yet sufficiently accurate prediction
of the mass spectra of small compounds.

Pseudo-code (it's all in the last ~100 lines of the "real" code):

    for all elements_in_formula do
        {
        for all atoms_of_element_in_formula do
            {
            for all individual_isotopes_in_element do
                {
                calculate new mass
                calculate new intensity
                adjust mass scale
                adjust intensity scale
                }
            }
        }


HISTORY

*Acknowledgement*: This program is based on a file named MASS.C, which one
of my (former) colleagues found around 1994 "somewhere on Internet". He
could not recall the source, and the file did neither carry any copyright
nor was the author identified somehow. As the original file was already
publicly available, I put this modified version under the GNU Public
License. If the original author recognises his work, I'd like to know who
he is :-)

I have brought different little modifications to the program, but the main
routines remained unchanged. The version numbers reflect the date of the last
modification in ISO style.

2005-06-17: Added command-line calculation and web interface stuff;
            License now exclusively GPL v2 (JHa)
2005-06-21: Extended ocumentation (JHa).


BUGS

If you find any bugs, please contact me by mail (see below). I try to
maintain the program as far as time and work allow.


AUTHOR

    Dr Joerg Hau
    Sur le Jordil
    CH-1148 Cuarnens
    Switzerland

    joerg.hau at dplanet.ch


Thanks you for your interest, and ... have fun!


--eof--
